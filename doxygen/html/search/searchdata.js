var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxy~",
  1: "_abcdefghiklmnopqrstuvw",
  2: "bcgs",
  3: "bcdefghimnoprstu",
  4: "_abcdefghijklmnopqrstuvwx~",
  5: "_abcdefgiklmnopqrstuvxy",
  6: "_abcdefghiklmnoprstuvwx",
  7: "dhmprstw",
  8: "acdefgiklmnopqrstuv",
  9: "_abcdeimnopqruv",
  10: "_bcdeghiklmnopru"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros"
};

