var searchData=
[
  ['q_5frec_0',['q_rec',['../namespacegraph__tool.html#acac6d2df226cfcd2cf5408c8122bfd0e',1,'graph_tool']]],
  ['q_5frec_5fmemo_1',['q_rec_memo',['../namespacegraph__tool.html#aca1169f4838eccbdd5b2bc10c843bac1',1,'graph_tool']]],
  ['quadtree_2',['QuadTree',['../classgraph__tool_1_1QuadTree.html#a73b5b3fa616cbd3408d0ba497e6ff395',1,'graph_tool::QuadTree::QuadTree()'],['../classgraph__tool_1_1QuadTree.html#a42007f1e7376bd66c54ed6b65c098f3b',1,'graph_tool::QuadTree::QuadTree(const Pos &amp;ll, const Pos &amp;ur, int max_level, size_t n)']]]
];
