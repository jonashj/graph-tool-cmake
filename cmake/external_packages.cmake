
include(FindPkgConfig)
include(FindThreads)
include(FetchContent)

if(${GRAPH_TOOL_ENVIRONMENT_INSTALL})
set(GRAPH_TOOL_PKGCONFIG_PATH ${Python3_STDLIB}/pkconfig)
set(GRAPH_TOOL_PYCAIRO_INCLUDE ${Python3_STDLIB}/pycairo/)
endif()

if(NOT DEFINED GRAPH_TOOL_PYCAIRO_INCLUDE)
    message(FATAL_ERROR "GRAPH_TOOL_PYCAIRO_INCLUDE not defined")
endif()

find_package(Boost COMPONENTS python numpy iostreams coroutine REQUIRED)



pkg_check_modules(Cairo REQUIRED cairo)

pkg_check_modules(Cairomm REQUIRED IMPORTED_TARGET cairomm-1.0)

pkg_check_modules(GMP REQUIRED gmp)

if(ENABLE_SPARSEHASH)
    pkg_check_modules(SPARSEHASH REQUIRED sparsehash)
endif()

