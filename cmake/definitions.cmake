list(APPEND compile_definitions OPENMP_MIN_THRESH=${OPENMP_MIN_THRESH})
set(PACKAGE_NAME "graph-tool")
set(PACKAGE_AUTHOR "Tiago Peixoto")
set(PACKAGE_COPYRIGHT "2011-2021 Tiago Peixoto")
set(PACKAGE_TARNAME "graph-tool")
set(PACKAGE_BUGREPORT "http://graph-tool.skewed.de/issues")
set(PACKAGE_URL "http://graph-tool.skewed.de")
set(PROJECT_VERSION "2.38")
set(BOOST_BIND_GLOBAL_PLACEHOLDERS 1)
set(BOOST_COROUTINE_STACK_SIZE 5242880)
set(GRAPH_TOOL_COPYRIGHT "Copyright (C) 2006-2022 Tiago de Paula Peixoto\nThis is free software; see the source for copying conditions.  There is NO\nwarranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.")

if(CMAKE_BUILD_TYPE EQUAL "Debug")
    set(GRAPH_TOOL_PREPROCESSOR_FLAGS "-DNEBUG")
endif()

# get git commit version
execute_process(COMMAND git rev-parse --short HEAD
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GRAPH_TOOL_GIT_COMMIT_HASH
    OUTPUT_STRIP_TRAILING_WHITESPACE)

# get git commit date
execute_process(COMMAND git show -s --format=%ci
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GRAPH_TOOL_GIT_COMMIT_DATE
    OUTPUT_STRIP_TRAILING_WHITESPACE)

if(ENABLE_OPENMP)
    find_package(OpenMP)

    if(OpenMP_CXX_FOUND)
        list(APPEND compile_definitions HAVE_OPENMP)
    endif()
endif()

if(ENABLE_VALGRIND)
    find_package(Valgrind)

    if(Valgrind_FOUND)
        list(APPEND compile_definitions HAVE_VALGRIND)
    endif()
endif()

include(CheckIncludeFile)
check_include_file(dlfcn.h HAVE_DLFCN_H)
include(CheckIncludeFileCXX)
check_include_file_cxx(inttypes.h HAVE_INTTYPES_H)
check_include_file_cxx(stdint.h HAVE_STDINT_H)
check_include_file_cxx(stdio.h HAVE_STDIO_H)
check_include_file_cxx(stdlib.h HAVE_STDLIB_H)
check_include_file_cxx(strings.h HAVE_STRINGS_H)
check_include_file_cxx("string.h" HAVE_STRING_H)
check_include_file_cxx(sys/stat.h HAVE_SYS_STAT_H)
check_include_file_cxx(sys/types.h HAVE_SYS_TYPES_H)
check_include_file_cxx(unistd.h HAVE_UNISTD_H)
check_include_file_cxx(wchar.h HAVE_WCHAR_H)
check_include_file_cxx(py3cairo.h HAVE_PY3CAIRO_H)

message(WARNING ${HAVE_PY3CAIRO_H})
if(${HAVE_DLFCN_H})
    list(APPEND config_compile_definitions HAVE_DLFCN_H)
endif()

if(${HAVE_INTTYPES_H})
    list(APPEND config_compile_definitions HAVE_INTTYPES_H)
endif()

if(${HAVE_STDINT_H})
    list(APPEND config_compile_definitions HAVE_STDINT_H)
endif()

if(${HAVE_STDIO_H})
    list(APPEND config_compile_definitions HAVE_STDIO_H)
endif()

if(${HAVE_STDLIB_H})
    list(APPEND config_compile_definitions HAVE_STDLIB_H)
endif()

if(${HAVE_STRINGS_H})
    list(APPEND config_compile_definitions HAVE_STRINGS_H)
endif()

if(${HAVE_STRING_H})
    list(APPEND config_compile_definitions HAVE_STRING_H)
endif()

if(${HAVE_SYS_STAT_H})
    list(APPEND config_compile_definitions HAVE_SYS_STAT_H)
endif()

if(${HAVE_SYS_TYPES_H})
    list(APPEND config_compile_definitions HAVE_SYS_TYPES_H)
endif()

if(${HAVE_UNISTD_H})
    list(APPEND config_compile_definitions HAVE_UNISTD_H)
endif()

if(${HAVE_WCHAR_H})
    list(APPEND config_compile_definitions HAVE_WCHAR_H)
endif()


# GCC_VERSION
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")

    execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion
        OUTPUT_VARIABLE GCC_VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    #convert gcc version to format 40400
    string(REGEX REPLACE "^([0-9]+)\\.([0-9]+)\\.([0-9]+)$" "\\1\\2\\3" GCC_VERSION ${GCC_VERSION})    
endif()

list(APPEND config_compile_definitions GCC_VERSION=${GCC_VERSION})

if(${ENABLE_OPENMP})
    list(APPEND config_compile_definitions HAVE_OPENMP)
endif()

if(${ENABLE_SPARSEHASH})
    list(APPEND config_compile_definitions HAVE_SPARSEHASH)
endif()

list(APPEND config_compile_definitions "HAVE_PYTHON=${Python3_VERSION}")
list(APPEND config_compile_definitions "BOOST_ALLOW_DEPRECATED_HEADERS=1")
list(APPEND config_compile_definitions "GRAPH_TOOLS_PYCAIRO_HEADER=${GRAPH_TOOLS_PYCAIRO_HEADER_PATH}")

configure_file(${PROJECT_SOURCE_DIR}/cmake/config.h.in ${PROJECT_BINARY_DIR}/src/graph/config.h)