set(GRAPH_TOOL_CMAKE_CONFIG_DIR "${CMAKE_CURRENT_BINARY_DIR}")
set(CMAKE_CURRENT_LIST_DIR ${CMAKE_CURRENT_BINARY_DIR})
set(GRAPH_TOOL_TARGET_NAMES ${graph_tool_TARGETS})

if(${GRAPH_TOOL_ENVIRONMENT_INSTALL})
set(GRAPH_TOOL_SITE_PACKAGE_DIR ${Python3_SITELIB})
set(GRAPH_TOOL_CONFIG_INSTALL_DIR "${GRAPH_TOOL_SITE_PACKAGE_DIR}/../../cmake/${PROJECT_NAME}" CACHE INTERNAL "")
set(GRAPH_TOOL_INCLUDE_INSTALL_DIR "${GRAPH_TOOL_SITE_PACKAGE_DIR}/${PROJECT_NAME}/include/graph_tool")
set(GRAPH_TOOL_LIBRARY_INSTALL_DIR "${GRAPH_TOOL_SITE_PACKAGE_DIR}/${PROJECT_NAME}")
else()
set(GRAPH_TOOL_SITE_PACKAGE_DIR ${CMAKE_INSTALL_PREFIX}/lib)
set(GRAPH_TOOL_CONFIG_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/lib/cmake/${PROJECT_NAME}" CACHE INTERNAL "")
set(GRAPH_TOOL_INCLUDE_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/include/graph_tool")
set(GRAPH_TOOL_LIBRARY_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/lib")
endif()

set(GRAPH_TOOL_TARGETS_EXPORT_NAME "${PROJECT_NAME}Targets")
set(GRAPH_TOOL_CMAKE_CONFIG_TEMPLATE "cmake/Config.cmake.in")
set(GRAPH_TOOL_CMAKE_VERSION_CONFIG_FILE "${GRAPH_TOOL_CMAKE_CONFIG_DIR}/${PROJECT_NAME}ConfigVersion.cmake")
set(GRAPH_TOOL_CMAKE_PROJECT_CONFIG_FILE "${GRAPH_TOOL_CMAKE_CONFIG_DIR}/${PROJECT_NAME}Config.cmake")
set(GRAPH_TOOL_CMAKE_PROJECT_TARGETS_FILE "${GRAPH_TOOL_CMAKE_CONFIG_DIR}/${PROJECT_NAME}Targets.cmake")

configure_file(
    "cmake/graph_toolConfigVersion.cmake.in"
    ${GRAPH_TOOL_CMAKE_VERSION_CONFIG_FILE}
    @ONLY
)

configure_file(${GRAPH_TOOL_CMAKE_CONFIG_TEMPLATE} ${GRAPH_TOOL_CMAKE_PROJECT_CONFIG_FILE} @ONLY)

install(
    DIRECTORY ${GRAPH_TOOL_INCLUDE_BUILD_DIR}
    DESTINATION ${GRAPH_TOOL_INCLUDE_INSTALL_DIR}
)

install(
    FILES ${GRAPH_TOOL_CMAKE_PROJECT_CONFIG_FILE} ${GRAPH_TOOL_CMAKE_VERSION_CONFIG_FILE}
    DESTINATION ${GRAPH_TOOL_CONFIG_INSTALL_DIR}
)

add_library(graph_tool INTERFACE)
target_include_directories(graph_tool INTERFACE $<INSTALL_INTERFACE:${GRAPH_TOOL_INCLUDE_INSTALL}/graph_tool/include>)
target_link_libraries(graph_tool INTERFACE graph_tool_core graph_tool_centrality graph_tool_clustering graph_tool_correlations graph_tool_draw graph_tool_dynamics graph_tool_flow graph_tool_generation graph_tool_inference graph_tool_search graph_tool_spectral graph_tool_stats graph_tool_topology graph_tool_util)
export(
    TARGETS graph_tool graph_tool_centrality graph_tool_config gt_pycairo_aux graph_tool_core graph_tool_include_interface graph_tool_clustering graph_tool_correlations graph_tool_draw graph_tool_dynamics graph_tool_flow graph_tool_generation graph_tool_inference graph_tool_search graph_tool_spectral graph_tool_stats graph_tool_topology graph_tool_util
    NAMESPACE graph_tool::
    FILE ${GRAPH_TOOL_CMAKE_PROJECT_TARGETS_FILE})

foreach(target centrality clustering correlations config draw dynamics flow generation inference search spectral stats topology util)
    install(
        TARGETS graph_tool_${target}
        EXPORT ${GRAPH_TOOL_TARGETS_EXPORT_NAME}
        INCLUDES DESTINATION ${GRAPH_TOOL_INCLUDE_INSTALL_DIR}/${target}/
        LIBRARY DESTINATION ${GRAPH_TOOL_LIBRARY_INSTALL_DIR}/${target}/
    )

    install(DIRECTORY "${PROJECT_SOURCE_DIR}/src/graph_tool/${target}" # source directory
        DESTINATION "${GRAPH_TOOL_INCLUDE_INSTALL_DIR}/${target}" # target directory
        FILES_MATCHING # install only matched files
        PATTERN "*.hh" # select header files)
    )
endforeach()

install(TARGETS graph_tool
    EXPORT ${GRAPH_TOOL_TARGETS_EXPORT_NAME}
    INCLUDES DESTINATION ${GRAPH_TOOL_INCLUDE_INSTALL_DIR}/
    LIBRARY DESTINATION ${GRAPH_TOOL_LIBRARY_INSTALL_DIR}/
)

install(TARGETS gt_pycairo_aux
    EXPORT ${GRAPH_TOOL_TARGETS_EXPORT_NAME}
    INCLUDES DESTINATION ${GRAPH_TOOL_INCLUDE_INSTALL_DIR}/draw/
    LIBRARY DESTINATION ${GRAPH_TOOL_LIBRARY_INSTALL_DIR}/draw/
)

install(DIRECTORY "${PROJECT_SOURCE_DIR}/src/graph/" # source directory
    DESTINATION "${GRAPH_TOOL_INCLUDE_INSTALL_DIR}" # target directory
    FILES_MATCHING # install only matched files
    PATTERN "*.hh" # select header files
)

install(
    DIRECTORY ${graph_tools_boost_workaround_INCLUDE_DIR}
    DESTINATION ${GRAPH_TOOL_INCLUDE_INSTALL_DIR}/boost_workaround
    FILES_MATCHING
    PATTERN "*.hpp"
)

install(
    DIRECTORY ${graph_tools_pcg_INCLUDE_DIR}
    DESTINATION ${GRAPH_TOOL_INCLUDE_INSTALL_DIR}/pcg-cpp
    FILES_MATCHING
    PATTERN "*.hpp"
)
install(
    FILES ${PROJECT_BINARY_DIR}/src/graph/config.h
    DESTINATION ${GRAPH_TOOL_INCLUDE_INSTALL_DIR}
)

install(TARGETS graph_tool_include_interface graph_tool_core
    EXPORT ${GRAPH_TOOL_TARGETS_EXPORT_NAME}
    INCLUDES DESTINATION ${GRAPH_TOOL_INCLUDE_INSTALL_DIR}/
    LIBRARY DESTINATION ${GRAPH_TOOL_LIBRARY_INSTALL_DIR}/
)

install(
    EXPORT ${GRAPH_TOOL_TARGETS_EXPORT_NAME}
    NAMESPACE graph_tool::
    DESTINATION ${GRAPH_TOOL_CONFIG_INSTALL_DIR}
)

if(${ENABLE_PYTHON_BINDER_MANUAL_INSTALL})
    install(DIRECTORY ${PROJECT_SOURCE_DIR}/src/graph_tool DESTINATION ${GRAPH_TOOL_SITE_PACKAGE_DIR})
endif()

# install(FILES ${EXPAT_LIBRARIES}
#     DESTINATION ${GRAPH_TOOL_SITE_PACKAGE_DIR}/../../
# )
# install(FILES ${GRAPH_TOOL_CAIRO_LIBS}
#     DESTINATION ${GRAPH_TOOL_SITE_PACKAGE_DIR}/../../)
