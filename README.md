graph-tool -- an efficient python module for analysis of graphs
================================================================

graph-tool is an efficient python module for manipulation and
statistical analysis of graphs. It contains several general graph
measurements, data structures and algorithms, such as vertex and edge
properties, online graph filtering, nearest neighbor statistics,
clustering, interactive graph layout, random graph generation, detection
of community structure, and more.

Contrary to most other python modules with similar functionality, the
core data structures and algorithms are implemented in C++, making
extensive use of template metaprogramming, based heavily on the Boost
Graph Library. This confers it a level of performance that is
comparable (both in memory usage and computation time) to that of a
pure C/C++ library.

For more information and documentation, please take a look at the
website http://graph-tool.skewed.de.

graph-tool is free software, you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License, version 3 or
above. See COPYING and COPYING.LESSER for details.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Availability
------------

The current stable version of graph-tool is always available from the
project's website: http://graph-tool.skewed.de

Installation
------------
```
mkdir build && cd build
```
Configure while sourced in desired Python environment:
```
cmake .. -DGRAPH_TOOL_ENVIRONMENT_INSTALL=ON
```
C++ build:
``` 
cmake --build . --parallel N_JOBS
```
C++ install:
```
cmake --build . --target install
```
Build pip wheel:
```
cmake --build . --target build_wheel
```
build & install pip wheel:
```
cmake --build . --target install_wheel
```


Notes
-----
- CMake package-files only comes with the default CMake install.
- Older dependencies utilize `pkgconfig`
- Requires a C++17 compiler (GCC version 7 or above, or clang version 5 or above).

Dependencies
------------------------
See https://git.skewed.de/count0/graph-tool/-/wikis/installation-instructions

- The Boost libraries, version 1.55 or above.

- Python version 3 or above.

- The expat XML library.

- The SciPy python module.

- The Numpy python module, version 1.7 or above.

- The CGAL C++ geometry library, version 3.5 or above.

- The sparsehash template library (optional, but recommended).

- The GTK+ 3, cairomm, pycairo and matplotlib libraries, used for graph drawing (optional).











