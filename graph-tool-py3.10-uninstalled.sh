# generated by configure / remove this line to disable regeneration
prefix="/usr/local"
exec_prefix="${prefix}"
bindir="${exec_prefix}/bin"
libdir="/home/man/Documents/graph-tool-cmake/src/.libs"
datarootdir="${prefix}/share"
datadir="${datarootdir}"
sysconfdir="${prefix}/etc"
includedir="/home/man/Documents/graph-tool-cmake/."
package="graph-tool"
suffix=""

for option; do case "$option" in --list-all|--name) echo  "graph-tool-py3.10"
;; --help) pkg-config --help ; echo Buildscript Of "graph-tool Python library"
;; --modversion|--version) echo "2.46dev"
;; --requires) echo : ""
;; --libs) echo -L${libdir} "" "-lboost_python3"
       :
;; --cflags) echo -I${includedir} "-I/usr/lib/python3.10/site-packages/graph_tool/include -I/usr/lib/python3.10/site-packages/graph_tool/include/boost-workaround -I/usr/lib/python3.10/site-packages/graph_tool/include/pcg-cpp -I/usr/lib/python3.10/site-packages/cairo/include -I/usr/include/python3.10 -pthread -I/usr/include -I/usr/lib/python3.10/site-packages/numpy/core/include"
       :
;; --variable=*) eval echo '$'`echo $option | sed -e 's/.*=//'`
;; --uninstalled) exit 0 
;; *) ;; esac done
