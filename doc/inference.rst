.. automodule:: graph_tool.inference
    :show-inheritance:
    :inherited-members:

..
   .. testcode:: inference_detailed
      :hide:

      import test_inference

   .. testoutput:: inference_detailed
      :hide:
      :options: -ELLIPSIS, +NORMALIZE_WHITESPACE

      OK

   .. testcode:: inference_mcmc_detailed
      :hide:

      import test_inference_mcmc

   .. testoutput:: inference_mcmc_detailed
      :hide:
      :options: -ELLIPSIS, +NORMALIZE_WHITESPACE

      OK

