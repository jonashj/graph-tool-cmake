function(append_subdir_sources outvar dirname)
file(GLOB_RECURSE ${V_TARGET}_SOURCE_FILES LIST_DIRECTORIES false "${dirname}/*.cpp" "${dirname}/*.c" "${dirname}/*.cc")
set(${outvar} ${outvar} ${V_TARGET}_SOURCE_FILES)
endfunction()



MACRO(SUBDIRLIST result curdir)
FILE(GLOB children RELATIVE ${curdir} ${curdir}/*)
SET(dirlist "")
FOREACH(child ${children})
IF(IS_DIRECTORY ${curdir}/${child})
LIST(APPEND dirlist ${child})
ENDIF()
ENDFOREACH()
SET(${result} ${dirlist})
ENDMACRO()

SUBDIRLIST(graph_tool_subdirs ${CMAKE_CURRENT_SOURCE_DIR})

add_library(graph_tool_include_interface INTERFACE)
target_include_directories(graph_tool_include_interface INTERFACE $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src/graph> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}>)
target_include_directories(graph_tool_include_interface INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}>)
target_include_directories(graph_tool_include_interface INTERFACE $<BUILD_INTERFACE:${graph_tools_boost_workaround_INCLUDE_DIR}> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}/boost_workaround>)
target_include_directories(graph_tool_include_interface INTERFACE $<BUILD_INTERFACE:${graph_tool_pcg_INCLUDE_DIR}> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}/pcg-cpp>)

target_link_libraries(graph_tool_include_interface INTERFACE graph_tool_config)
file(GLOB graph_tool_core_SOURCES LIST_DIRECTORIES false "*.cpp" "*.c" "*.cc")
add_library(graph_tool_core SHARED  ${graph_tool_core_SOURCES} ${graph_tool_pcg_HEADERS})
target_compile_definitions(graph_tool_core PUBLIC ${compile_definitions})
target_compile_options(graph_tool_core PUBLIC ${GRAPH_TOOL_COMPILE_OPTIONS_DEFAULT})
target_include_directories(graph_tool_core PRIVATE $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src/graph> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}>)
target_link_libraries(graph_tool_core PUBLIC graph_tool_include_interface Boost::python Boost::numpy Boost::iostreams Python3::Python Python3::NumPy)
set_target_properties(graph_tool_core PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/graph_tool)
file(GLOB core_sources LIST_DIRECTORIES false "*.cpp" "*.c" "*.cc")
file(GLOB core_headers LIST_DIRECTORIES false "*.hpp" "*.h" "*.hh")

foreach(V_DIR ${graph_tool_subdirs})
append_subdir_sources(graph_tool_SOURCE_FILES ${V_DIR})
endforeach()
set(graph_tool_TARGETS graph_tool_centrality graph_tool_clustering graph_tool_correlations graph_tool_draw graph_tool_dynamics graph_tool_flow graph_tool_generation graph_tool_inference graph_tool_layout graph_tool_search graph_tool_spectral graph_tool_stats graph_tool_topology graph_tool_util)
foreach(V_TARGET ${graph_tool_subdirs})
file(GLOB_RECURSE ${V_TARGET}_SOURCE_FILES LIST_DIRECTORIES false "${V_TARGET}/*.cpp" "${V_TARGET}/*.c" "${V_TARGET}/*.cc")
file(GLOB_RECURSE ${V_TARGET}_HEADER_FILES LIST_DIRECTORIES false "${V_TARGET}/*.hpp" "${V_TARGET}/*.h" "${V_TARGET}/*.hh")

if(NOT V_TARGET STREQUAL ".deps")
if(NOT V_TARGET STREQUAL "draw")
add_library(graph_tool_${V_TARGET} SHARED ${${V_TARGET}_SOURCE_FILES} ${${V_TARGET}_HEADER_FILES})
target_compile_definitions(graph_tool_${V_TARGET} PUBLIC ${compile_definitions})
target_include_directories(graph_tool_${V_TARGET} PRIVATE $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src/graph> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}>)
target_include_directories(graph_tool_${V_TARGET} PRIVATE $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src/graph/${V_TARGET}/> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}/${V_TARGET}>)
target_link_libraries(graph_tool_${V_TARGET} PUBLIC graph_tool_include_interface)
target_link_libraries(graph_tool_${V_TARGET} PRIVATE Threads::Threads graph_tool_core Boost::python Boost::numpy Boost::iostreams Boost::coroutine Python3::Python Python3::NumPy)
target_compile_options(graph_tool_${V_TARGET} PUBLIC ${GRAPH_TOOL_COMPILE_OPTIONS_DEFAULT})
set_target_properties(graph_tool_${V_TARGET} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/graph_tool/${V_TARGET})
endif()
endif()
endforeach()


set(graph_tool_draw_SOURCE_FILES ${PROJECT_SOURCE_DIR}/src/graph/draw/graph_cairo_draw.cc ${PROJECT_SOURCE_DIR}/src/graph/draw/graph_tree_cts.cc)
add_library(graph_tool_draw SHARED ${graph_tool_draw_SOURCE_FILES} ${draw_HEADER_FILES})
target_compile_definitions(graph_tool_draw PUBLIC ${compile_definitions})
target_include_directories(graph_tool_draw PRIVATE $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src/graph> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}>)
target_include_directories(graph_tool_draw PRIVATE $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src/graph/draw/> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}/draw>)
target_link_libraries(graph_tool_draw PUBLIC graph_tool_include_interface)
target_link_libraries(graph_tool_draw PRIVATE Threads::Threads graph_tool_core Boost::python Boost::numpy Boost::iostreams Boost::coroutine Python3::Python Python3::NumPy)
target_compile_options(graph_tool_draw PUBLIC ${GRAPH_TOOL_COMPILE_OPTIONS_DEFAULT})
set_target_properties(graph_tool_draw PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/graph_tool/draw)

target_link_libraries(graph_tool_draw PRIVATE ${GRAPH_TOOL_CAIRO_LIBS})
target_include_directories(graph_tool_draw PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src/pycairo> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}/pycairo>)
target_include_directories(graph_tool_draw PRIVATE ${GRAPH_TOOL_CAIRO_INCLUDES})

set_target_properties(graph_tool_layout PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/graph_tool/draw)

add_library(gt_pycairo_aux SHARED ${PROJECT_SOURCE_DIR}/src/graph/draw/pycairo_aux.cc)
target_compile_definitions(gt_pycairo_aux PUBLIC ${compile_definitions} HAVE_CAIROMM=1)
target_include_directories(gt_pycairo_aux PRIVATE ${GRAPH_TOOL_CAIRO_INCLUDES} ${GRAPH_TOOL_PYCAIRO_INCLUDE})
target_include_directories(gt_pycairo_aux PRIVATE $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src/graph> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}>)
target_include_directories(gt_pycairo_aux PRIVATE $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src/graph/draw/> $<INSTALL_INTERFACE:${GRAPH_TOOL_HEADER_INSTALL_DIR}/draw>)
target_link_libraries(gt_pycairo_aux PUBLIC graph_tool_include_interface)
target_link_libraries(gt_pycairo_aux PRIVATE Threads::Threads graph_tool_core Boost::python Boost::numpy Boost::iostreams Boost::coroutine Python3::Python Python3::NumPy ${GRAPH_TOOL_CAIRO_LIBS})
target_link_directories(gt_pycairo_aux PRIVATE ${GRAPH_TOOL_CAIRO_LIB_DIRS})
target_compile_options(gt_pycairo_aux PUBLIC ${GRAPH_TOOL_COMPILE_OPTIONS_DEFAULT})



set(graph_tool_TARGETS ${graph_tool_TARGETS} graph_tool_core PARENT_SCOPE)

file(GLOB GRAPH_TOOL_BUILD_BINARIES "*.so" "*.dll" "*.dylib")

